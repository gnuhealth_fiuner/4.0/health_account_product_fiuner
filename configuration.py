from trytond import backend
from trytond.model import fields, ModelSQL, ValueMixin
from trytond.tools.multivalue import migrate_property
from trytond.pool import PoolMeta

default_product_category = fields.Many2One('product.category',
    "Default product category",
    help="The default product category for new products.",
    domain=[('accounting', '=', True)])

class Configuration(metaclass=PoolMeta):
    __name__ = 'product.configuration'

    default_product_category = fields.MultiValue(default_product_category)

class ConfigurationDefaultProductCategory(ModelSQL, ValueMixin):
    "Product Configuration Default Product Category"
    __name__ = 'product.configuration.default_product_category'

    default_product_category = default_product_category

    @classmethod
    def __register__(cls, module_name):
        exist = backend.TableHandler.table_exist(cls._table)

        super(ConfigurationDefaultProductCategory, cls).__register__(
            module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('default_product_category')
        value_names.append('default_product_category')
        migrate_property(
            'product.configuration', field_names, cls, value_names,
            fields=fields) 
