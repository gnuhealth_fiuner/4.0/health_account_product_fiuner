from trytond.pool import Pool

from . import configuration
from . import product

def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationDefaultProductCategory,
        product.Template,
        module='health_account_product_fiuner', type_='model')
