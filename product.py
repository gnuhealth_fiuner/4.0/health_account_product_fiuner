from trytond.pool import PoolMeta, Pool
from trytond.model import fields

class Template(metaclass=PoolMeta):
    __name__ = 'product.template'

    @fields.depends('purchasable')
    def on_change_with_account_category(self, name=None):
        pool = Pool()
        Config = pool.get('product.configuration')(1)
        default_product_category = Config.default_product_category
        if (self.purchasable) and default_product_category:
            return default_product_category.id
        return None
